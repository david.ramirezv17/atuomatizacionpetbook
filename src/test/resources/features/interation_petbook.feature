# language: es
Característica: Filtrar Mascotas
  Como persona usuaria de PetBook
  Quiero visualizar fotos de perros y gatos.
  Para ver cúal de ellos me gusta para adoptar.

	Escenario: Ver todos los animales de PetBook
  	Cuando Camilo ingrese a la página de Petbook
    Entonces Visualizar las fotos de todos los animales de la aplicación
   
   Escenario: Ver los gatos de Petbook
    Cuando Camilo haga click en el botón Gato
    Entonces Debe visualizar únicamente los gatos de la aplicación

   Escenario: Ver los perros de Petbook
    Cuando Camilo haga click en el botón Perro
    Entonces Debe visualizar únicamente los perros de la aplicación
    
   Escenario: Ver foto ampliada
    Cuando Camilo haga click en la foto del perro o gato
    Entonces Debe ver de manera ampliada la foto del animal
    
   