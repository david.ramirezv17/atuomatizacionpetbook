package co.com.devco.stepdefinitions;

import co.com.petbook.questions.*;
import co.com.petbook.tasks.*;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;
import static org.hamcrest.core.IsEqual.equalTo;

public class InterationPetbookDefinitions {

	@Cuando("^(.*) ingrese a la página de Petbook$")
    public void realizarInterationPetbook(String cliente) {
        theActorCalled(cliente).attemptsTo(
                Navegar.a("All")
        );
    }//(.*) lo setea en el string Cliente

    @Entonces("^Visualizar las fotos de todos los animales de la aplicación$")
    public void debeVerTodasLasFotosDeTodosLosAnimales() {
        theActorInTheSpotlight().should(seeThat(ValorAll.nombre(), equalTo("fotos de todos los animales de la aplicación")));
    }
    
	@Cuando("^(.*) haga click en el botón Gato$")
    public void realizarInterationPetbookGato(String cliente) {
        theActorCalled(cliente).attemptsTo(
                Navegar.a("Gato")
        );
    }

    @Entonces("^Debe visualizar únicamente los gatos de la aplicación$")
    public void debeVerTodasLasFotosDeGatos() {
        theActorInTheSpotlight().should(seeThat(ValorGato.nombre(), equalTo("Foto De Gatos")));
    }
    
    @Cuando("^(.*) haga click en el botón Perro$")
    public void camiloHagaClickEnElBotónPerro(String cliente) {
    	theActorCalled(cliente).attemptsTo(
                Navegar2.a("Perro")
        );
    }

    @Entonces("^Debe visualizar únicamente los perros de la aplicación$")
    public void debeVisualizarÚnicamenteLosPerrosDeLaAplicación() {
    	theActorInTheSpotlight().should(seeThat(ValorPerro.nombre(), equalTo("Foto De Perros")));
    }
    
    @Cuando("^(.*) haga click en la foto del perro o gato$")
    public void camiloHagaClickEnLaFotoDelPerroOGato(String cliente) {
        theActorCalled(cliente).attemptsTo(
                Navegar3.a("Gato")
        );
    }

    @Entonces("^Debe ver de manera ampliada la foto del animal$")
    public void debeRecibirUnaRespuestaAdecuadaASuContacto() {
    	theActorInTheSpotlight().should(seeThat(ValorGato.nombre(), equalTo("Foto De Gatos")));
    }
    
}
