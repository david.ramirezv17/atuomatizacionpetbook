package co.com.petbook.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.waits.WaitUntil;

import static co.com.petbook.userinterfaces.PetbookMainPage.*;
import static net.serenitybdd.screenplay.matchers.WebElementStateMatchers.isVisible;

public class Navegar3 implements Task {
    private String menu;
    public Navegar3(String menu)
    {
        this.menu=menu;
    }
    public static Performable a(String menu)
    {
        return new Navegar3(menu);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.wasAbleTo(
                Open.url(PAGINA_PETBOOK),
                Click.on(opcion_menu(this.menu)),
                WaitUntil.the(GATO,isVisible()),
                Click.on(opcion_menu(FOTOGATO.toString())),
                WaitUntil.the(FOTOGATO,isVisible())
                
        );
        
        
        
    }
}
