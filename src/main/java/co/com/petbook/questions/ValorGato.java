package co.com.petbook.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TheValue;
import static co.com.petbook.userinterfaces.PetbookMainPage.*;

public class ValorGato implements Question<String> {

    public static ValorGato nombre(){
        return new ValorGato();
    }

    @Override
    public String answeredBy(Actor actor) {
        return TheValue.of(FOTOGATO).answeredBy(actor).toString();
    }
}
