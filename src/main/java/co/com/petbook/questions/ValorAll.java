package co.com.petbook.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TheValue;
import static co.com.petbook.userinterfaces.PetbookMainPage.*;

public class ValorAll implements Question<String> {

    public static ValorAll nombre(){
        return new ValorAll();
    }

    @Override
    public String answeredBy(Actor actor) {
        return TheValue.of(FOTOALL).answeredBy(actor).toString();
    }
}
