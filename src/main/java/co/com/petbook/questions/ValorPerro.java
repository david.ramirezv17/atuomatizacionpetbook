package co.com.petbook.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.TheValue;
import static co.com.petbook.userinterfaces.PetbookMainPage.*;

public class ValorPerro implements Question<String> {

    public static ValorPerro nombre(){
        return new ValorPerro();
    }

    @Override
    public String answeredBy(Actor actor) {
        return TheValue.of(FOTOPERRO).answeredBy(actor).toString();
    }
}
