package co.com.petbook.userinterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class PetbookMainPage {
	
	//Variables Petbook
	public static final String PAGINA_PETBOOK = "http://davidramirezpetbook.s3-website-us-east-1.amazonaws.com/";
	public static final Target ALL=Target.the("botón All").located(By.xpath("//button[text()='All']"));
	public static final Target PERRO=Target.the("botón Perro").located(By.xpath("//button[text()='Perro']"));
	public static final Target GATO=Target.the("botón Gato").located(By.xpath("//button[text()='Gato']"));
	public static final Target FOTOGATO=Target.the("Foto De Gatos").located(By.xpath("//button[text()='Gato']"));
	public static final Target FOTOPERRO=Target.the("Foto De Perros").located(By.xpath("//button[text()='Perro']"));
	public static final Target FOTOALL=Target.the("fotos de todos los animales de la aplicación").located(By.xpath("//button[text()='All']"));



//XPATH //button[text()='Perro'] //button[text()='Gato'] //button[text()='All']


	public static Target opcion_menu(String nombreMenu) {
		if ("gato".equalsIgnoreCase(nombreMenu)) {
			return GATO;
		} else if ("perro".equalsIgnoreCase(nombreMenu)) {
			return PERRO;
		} else if("fotogato".equalsIgnoreCase(nombreMenu)){
			return FOTOGATO;
		}else {
			return ALL;
		}
	}
}
